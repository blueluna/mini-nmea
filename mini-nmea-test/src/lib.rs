pub use mini_nmea::Error;

#[cfg(test)]
mod tests {
    use mini_nmea::Error;
    use mini_nmea_macro::FieldUpperCase;
    use std::convert::TryFrom;

    #[derive(Clone, Debug, FieldUpperCase, PartialEq)]
    enum Ta {
        Aaa,
        Bbb,
        Ccc,
    }

    #[test]
    fn it_works() {
        let ta = Ta::Aaa;
        assert_eq!(ta, Ta::Aaa);
        assert_eq!(format!("{}", ta), "AAA");
        let tb = Ta::Bbb;
        assert_eq!(format!("{}", tb), "BBB");
        let tc = Ta::Ccc;
        assert_eq!(format!("{}", tc), "CCC");
        let ta = Ta::try_from("AAA".as_bytes()).unwrap();
        assert_eq!(ta, Ta::Aaa);
    }
}
