use proc_macro2::{Span, TokenStream};
use quote::quote;
use std::string::ToString;
use syn::{parse_macro_input, spanned::Spanned, Data, DeriveInput};

#[proc_macro_derive(FieldUpperCase)]
pub fn field_upper_case(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    let input = parse_macro_input!(input as DeriveInput);
    let span = input.span();
    let enum_name = input.ident;
    let enum_data = if let Data::Enum(it) = input.data {
        it
    } else {
        return syn::Error::new(span, "Expected an `enum`")
            .to_compile_error()
            .into();
    };
    let mut dst = TokenStream::new();
    let variants_name: Vec<syn::Ident> = enum_data
        .variants
        .into_iter()
        .map(|variant| variant.ident)
        .collect();
    let variants_strlit: Vec<syn::LitStr> = variants_name
        .iter()
        .map(|ident| {
            let text = ident.to_string().to_uppercase();
            syn::LitStr::new(&text, Span::call_site())
        })
        .collect();

    let mut display = TokenStream::new();
    display.extend(quote! {
      impl core::fmt::Display for #enum_name {
          fn fmt(&self, f: &mut core::fmt::Formatter) -> core::fmt::Result {
              match self {
                    #( Self::#variants_name => write!(f, #variants_strlit) ),*
              }
          }
      }
    });
    dst.extend(display);

    let variant_byteslit: Vec<syn::LitByteStr> = variants_name
        .iter()
        .map(|ident| {
            let text = ident.to_string().to_uppercase();
            syn::LitByteStr::new(text.as_bytes(), Span::call_site())
        })
        .collect();

    let mut try_from = TokenStream::new();
    try_from.extend(quote! {
        impl TryFrom<&[u8]> for #enum_name {
            type Error = crate::Error;

            fn try_from(value: &[u8]) -> Result<Self, Error> {
                let value = match value {
                    #( #variant_byteslit => Self::#variants_name ),*,
                    _ => { return Err(Error::InvalidValue) }
                };
                Ok(value)
            }
        }
    });
    try_from.extend(quote! {
        impl TryFrom<&str> for #enum_name {
            type Error = crate::Error;

            fn try_from(value: &str) -> Result<Self, Error> {
                let value = match value {
                    #( #variants_strlit => Self::#variants_name ),*,
                    _ => { return Err(Error::InvalidValue) }
                };
                Ok(value)
            }
        }
    });
    dst.extend(try_from);
    dst.into()
}
