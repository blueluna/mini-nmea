use super::to_string;
use crate::Error;
use core::str::FromStr;

pub fn parse_number<T: FromStr>(bytes: &[u8]) -> Result<T, Error> {
    if bytes.is_empty() {
        return Err(Error::EmptyField);
    }
    let string = to_string(bytes)?;
    string.parse::<T>().map_err(|_| Error::ParseFailed)
}

pub fn parse_number_maybe<T: FromStr>(bytes: &[u8]) -> Result<Option<T>, Error> {
    match parse_number(bytes) {
        Ok(v) => Ok(Some(v)),
        Err(Error::EmptyField) => Ok(None),
        Err(error) => Err(error),
    }
}
