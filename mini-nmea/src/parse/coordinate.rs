use super::parse_number;
use crate::Error;

pub fn parse_direction_field(field: &[u8], latitude: bool) -> Result<f64, Error> {
    if field.is_empty() {
        return Err(Error::NotEnoughData);
    }
    let sign = if latitude {
        match field[0] {
            b'N' => 1.0,
            b'S' => -1.0,
            _ => return Err(Error::InvalidValue),
        }
    } else {
        match field[0] {
            b'E' => 1.0,
            b'W' => -1.0,
            _ => return Err(Error::InvalidValue),
        }
    };
    Ok(sign)
}

fn parse_coordinate(deg_field: &[u8], dir_field: &[u8], latitude: bool) -> Result<f64, Error> {
    if deg_field.len() < 7 {
        return Err(Error::NotEnoughData);
    }
    let sign = parse_direction_field(dir_field, latitude)?;
    let num_degree_characters = if latitude { 2 } else { 3 };
    let degrees = parse_number::<u8>(&deg_field[..num_degree_characters])?;
    let minutes = parse_number::<f64>(&deg_field[num_degree_characters..])?;
    let degrees = sign * (f64::from(degrees) + (minutes / 60.0));
    Ok(degrees)
}

pub fn parse_latitude(deg_field: &[u8], dir_field: &[u8]) -> Result<f64, Error> {
    parse_coordinate(deg_field, dir_field, true)
}

pub fn parse_longitude(deg_field: &[u8], dir_field: &[u8]) -> Result<f64, Error> {
    parse_coordinate(deg_field, dir_field, false)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn parse_latitude_ok() {
        let latitude = parse_latitude(&b"5167.78212"[..], &b"N"[..]).unwrap();
        assert_eq!(52.129702, latitude);
        let latitude = parse_latitude(&b"1562.83626"[..], &b"S,"[..]).unwrap();
        assert_eq!(-16.047271, latitude);
    }

    #[test]
    fn parse_latitude_err() {
        match parse_latitude(&b"5824.66521"[..], &b"E"[..]) {
            Ok(_) => assert!(false),
            Err(e) => {
                assert_eq!(e, Error::InvalidValue);
            }
        }
        match parse_latitude(&b"5824.66521"[..], &b"W"[..]) {
            Ok(_) => assert!(false),
            Err(e) => {
                assert_eq!(e, Error::InvalidValue);
            }
        }
        match parse_latitude(&b"AAAA.BBBBB"[..], &b"S"[..]) {
            Ok(_) => assert!(false),
            Err(e) => {
                assert_eq!(e, Error::ParseFailed);
            }
        }
        match parse_latitude(&b"5824.66521"[..], &b""[..]) {
            Ok(_) => assert!(false),
            Err(e) => {
                assert_eq!(e, Error::NotEnoughData);
            }
        }
        match parse_latitude(&b".66521"[..], &b"N"[..]) {
            Ok(_) => assert!(false),
            Err(e) => {
                assert_eq!(e, Error::NotEnoughData);
            }
        }
    }

    #[test]
    fn parse_longitude_ok() {
        let longitude = parse_longitude(&b"01492.27628"[..], &b"E"[..]).unwrap();
        assert_eq!(15.537938, longitude);
        let longitude = parse_longitude(&b"01176.82370"[..], &b"W"[..]).unwrap();
        assert_eq!(-12.280395, longitude);
    }

    #[test]
    fn parse_longitude_err() {
        match parse_longitude(&b"05824.66521"[..], &b"S"[..]) {
            Ok(_) => assert!(false),
            Err(e) => {
                assert_eq!(e, Error::InvalidValue);
            }
        }
        match parse_longitude(&b"05824.66521"[..], &b"N"[..]) {
            Ok(_) => assert!(false),
            Err(e) => {
                assert_eq!(e, Error::InvalidValue);
            }
        }
        match parse_longitude(&b"AAAA.BBBBB"[..], &b"W"[..]) {
            Ok(_) => assert!(false),
            Err(e) => {
                assert_eq!(e, Error::ParseFailed);
            }
        }
        match parse_longitude(&b"05824.66521"[..], &b""[..]) {
            Ok(_) => assert!(false),
            Err(e) => {
                assert_eq!(e, Error::NotEnoughData);
            }
        }
        match parse_longitude(&b".66521"[..], &b"E"[..]) {
            Ok(_) => assert!(false),
            Err(e) => {
                assert_eq!(e, Error::NotEnoughData);
            }
        }
        match parse_longitude(&b"1.66521"[..], &b"E"[..]) {
            Ok(_) => assert!(false),
            Err(e) => {
                assert_eq!(e, Error::ParseFailed);
            }
        }
    }
}
