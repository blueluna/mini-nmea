use super::parse_number;
use crate::Error;
use chrono::{Date, NaiveTime, TimeZone, Utc};

pub fn parse_hmsf(bytes: &[u8]) -> Result<NaiveTime, Error> {
    if bytes.len() < 6 {
        return Err(Error::NotEnoughData);
    }
    let hours = parse_number::<u8>(&bytes[0..2])?;
    let minutes = parse_number::<u8>(&bytes[2..4])?;
    let seconds = parse_number::<u8>(&bytes[4..6])?;
    let microseconds = if bytes.len() > 6 && bytes[6] == b'.' {
        let digits = 6 - (bytes.len() as isize - 7);
        let multiplier = match digits {
            0 => 1,
            1 => 10,
            2 => 100,
            3 => 1000,
            4 => 10000,
            5 => 100000,
            6 => 1000000,
            _ => return Err(Error::MalformedSentence),
        };
        let fraction = parse_number::<u32>(&bytes[7..])?;
        fraction * multiplier
    } else {
        0
    };
    match NaiveTime::from_hms_micro_opt(hours as u32, minutes as u32, seconds as u32, microseconds)
    {
        Some(time) => Ok(time),
        None => Err(Error::InvalidValue),
    }
}

pub fn parse_ymd(bytes: &[u8]) -> Result<Date<Utc>, Error> {
    if bytes.len() < 6 {
        return Err(Error::NotEnoughData);
    }
    let day = parse_number::<u8>(&bytes[0..2])?;
    let month = parse_number::<u8>(&bytes[2..4])?;
    let year = parse_number::<u8>(&bytes[4..6])?;
    if month > 12 || day > 31 {
        return Err(Error::InvalidValue);
    }
    let year = if year < 80 {
        u16::from(year) + 2000
    } else {
        u16::from(year) + 1900
    };
    Ok(Utc.ymd(year as i32, month as u32, day as u32))
}

#[cfg(test)]
mod tests {
    use super::*;
    use chrono::{Datelike, Timelike};

    #[test]
    pub fn test_parse_hmsf() {
        let time = parse_hmsf(&b"181032.102"[..]).unwrap();
        assert_eq!(18, time.hour());
        assert_eq!(10, time.minute());
        assert_eq!(32, time.second());
        assert_eq!(102000000, time.nanosecond());
        let time = parse_hmsf(&b"235959"[..]).unwrap();
        assert_eq!(23, time.hour());
        assert_eq!(59, time.minute());
        assert_eq!(59, time.second());
        assert_eq!(0, time.nanosecond());
        let time = parse_hmsf(&b"000000.00"[..]).unwrap();
        assert_eq!(0, time.hour());
        assert_eq!(0, time.minute());
        assert_eq!(0, time.second());
        assert_eq!(0, time.nanosecond());
        let time = parse_hmsf(&b"121212.01"[..]).unwrap();
        assert_eq!(12, time.hour());
        assert_eq!(12, time.minute());
        assert_eq!(12, time.second());
        assert_eq!(10000000, time.nanosecond());
        let time = parse_hmsf(&b"121212.000001"[..]).unwrap();
        assert_eq!(12, time.hour());
        assert_eq!(12, time.minute());
        assert_eq!(12, time.second());
        assert_eq!(1000, time.nanosecond());
        let result = parse_hmsf(&b"121212.0000001"[..]);
        assert_eq!(Err(Error::MalformedSentence), result);
        let result = parse_hmsf(&b"241010"[..]);
        assert_eq!(Err(Error::InvalidValue), result);
        let result = parse_hmsf(&b"236010"[..]);
        assert_eq!(Err(Error::InvalidValue), result);
        let result = parse_hmsf(&b"235961"[..]);
        assert_eq!(Err(Error::InvalidValue), result);
    }

    #[test]
    pub fn test_parse_ymd() {
        let date = parse_ymd(&b"031121"[..]).unwrap();
        assert_eq!(2021, date.year());
        assert_eq!(11, date.month());
        assert_eq!(3, date.day());
    }
}
