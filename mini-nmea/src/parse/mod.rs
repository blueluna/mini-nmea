use crate::Error;
use core::iter::Iterator;

mod coordinate;
mod date_time;
mod hexadecimal;
mod numbers;

pub use coordinate::{parse_direction_field, parse_latitude, parse_longitude};
pub use date_time::{parse_hmsf, parse_ymd};
pub use hexadecimal::hex_byte;
pub use numbers::{parse_number, parse_number_maybe};

pub fn to_string(bytes: &[u8]) -> Result<&str, Error> {
    core::str::from_utf8(bytes).map_err(|_| Error::InvalidString)
}

pub fn field_end(bytes: &[u8]) -> Result<usize, Error> {
    for (pos, byte) in bytes.iter().enumerate() {
        if *byte == b',' || *byte == b'*' {
            return Ok(pos);
        }
    }
    Err(Error::MalformedSentence)
}

fn field(bytes: &[u8], offset: usize) -> Result<(&[u8], usize), Error> {
    let count = field_end(&bytes[offset..])?;
    let end = offset + count;
    let field = &bytes[offset..end];
    Ok((field, end + 1))
}

pub struct FieldIterator<'a> {
    data: &'a [u8],
    pos: usize,
}

impl<'a> FieldIterator<'a> {
    pub fn from_bytes(bytes: &'a [u8]) -> FieldIterator<'a> {
        FieldIterator {
            data: bytes,
            pos: 0,
        }
    }

    pub fn next_err(&mut self) -> Result<&'a [u8], Error> {
        self.next().ok_or(Error::MalformedSentence)
    }
}

impl<'a> Iterator for FieldIterator<'a> {
    type Item = &'a [u8];
    // The method that generates each item
    fn next(&mut self) -> Option<Self::Item> {
        match field(self.data, self.pos) {
            Ok((d, p)) => {
                self.pos = p;
                Some(d)
            }
            Err(_) => None,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    pub fn field_iterator() {
        let iterator = FieldIterator::from_bytes(&b"AAA,0000,,*"[..]);
        for (n, field) in iterator.enumerate() {
            match n {
                0 => assert_eq!(field, b"AAA"),
                1 => assert_eq!(field, b"0000"),
                2 => assert_eq!(field, b""),
                3 => assert_eq!(field, b""),
                _ => assert!(false),
            }
        }
    }
}
