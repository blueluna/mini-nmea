/// Parse byte as hexadecimal character to a nibble
pub fn hex_nibble(value: u8) -> Option<u8> {
    let value = match value {
        b'0' => 0,
        b'1' => 1,
        b'2' => 2,
        b'3' => 3,
        b'4' => 4,
        b'5' => 5,
        b'6' => 6,
        b'7' => 7,
        b'8' => 8,
        b'9' => 9,
        b'a' | b'A' => 10,
        b'b' | b'B' => 11,
        b'c' | b'C' => 12,
        b'd' | b'D' => 13,
        b'e' | b'E' => 14,
        b'f' | b'F' => 15,
        _ => return None,
    };
    Some(value)
}

/// Parse two bytes as hexadecimal characters to a byte
pub fn hex_byte(value: &[u8]) -> Option<u8> {
    if value.len() < 2 {
        return None;
    }
    match (hex_nibble(value[0]), hex_nibble(value[1])) {
        (Some(high), Some(low)) => Some(high << 4 | low),
        _ => None,
    }
}

#[cfg(test)]
mod tests {
    use super::{hex_byte, hex_nibble};

    #[test]
    fn hex_nibbles() {
        for byte in 0..255u8 {
            if byte.is_ascii_hexdigit() {
                let bytes = &[byte];
                let string = core::str::from_utf8(bytes).unwrap();
                let value = u8::from_str_radix(string, 16).unwrap();
                assert_eq!(hex_nibble(byte), Some(value));
            } else {
                assert_eq!(hex_nibble(byte), None);
            }
        }
    }

    #[test]
    fn hex_bytes() {
        assert_eq!(hex_byte(b""), None);
        assert_eq!(hex_byte(b"A"), None);
        assert_eq!(hex_byte(b"BA"), Some(0xba));
        assert_eq!(hex_byte(b"fE"), Some(0xfe));
        assert_eq!(hex_byte(b"Da"), Some(0xda));
        assert_eq!(hex_byte(b"98"), Some(0x98));
        assert_eq!(hex_byte(b"01Q"), Some(0x01));
        assert_eq!(hex_byte(b"0Q"), None);
        assert_eq!(hex_byte(b"Q"), None);
    }
}
