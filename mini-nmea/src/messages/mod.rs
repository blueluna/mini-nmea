//! # NMEA message
//!
//!
//!

mod common;
mod gga;
mod gll;
mod gsa;
mod gst;
mod gsv;
mod message;
mod rmc;
mod vtg;

use crate::{Error, MessageId, Sentence};
pub use common::{GnssQualityIndicator, ModeIndicator, NauticalSpeed, Speed, Status};
pub use gga::Gga;
pub use gll::Gll;
pub use gsa::Gsa;
pub use gst::Gst;
pub use gsv::{Gsv, Satellite};
pub use message::FromSentence;
pub use rmc::Rmc;
pub use vtg::{MagneticTrack, TrueTrack, Vtg};

/// Parsed message content
pub enum MessageContent {
    /// GGA
    Gga(Gga),
    /// GLL
    Gll(Gll),
    /// GSA
    Gsa(Gsa),
    /// GST
    Gst(Gst),
    /// GSV
    Gsv(Gsv),
    /// RMC
    Rmc(Rmc),
    /// VTG
    Vtg(Vtg),
}

impl FromSentence for MessageContent {
    fn from_sentence(sentence: &Sentence) -> Result<Self, Error> {
        let message = match sentence.message_id {
            Some(MessageId::Gga) => Self::Gga(Gga::from_sentence(sentence)?),
            Some(MessageId::Gll) => Self::Gll(Gll::from_sentence(sentence)?),
            Some(MessageId::Gsa) => Self::Gsa(Gsa::from_sentence(sentence)?),
            Some(MessageId::Gsv) => Self::Gsv(Gsv::from_sentence(sentence)?),
            Some(MessageId::Gst) => Self::Gst(Gst::from_sentence(sentence)?),
            Some(MessageId::Rmc) => Self::Rmc(Rmc::from_sentence(sentence)?),
            Some(MessageId::Vtg) => Self::Vtg(Vtg::from_sentence(sentence)?),
            _ => {
                return Err(Error::InvalidMessageIdentifier);
            }
        };
        Ok(message)
    }
}

impl core::fmt::Display for MessageContent {
    fn fmt(&self, f: &mut core::fmt::Formatter) -> core::fmt::Result {
        match self {
            Self::Gga(msg) => {
                write!(f, "{}", msg)
            }
            Self::Gll(msg) => {
                write!(f, "{}", msg)
            }
            Self::Gsa(msg) => {
                write!(f, "{}", msg)
            }
            Self::Gst(msg) => {
                write!(f, "{}", msg)
            }
            Self::Gsv(msg) => {
                write!(f, "{}", msg)
            }
            Self::Rmc(msg) => {
                write!(f, "{}", msg)
            }
            Self::Vtg(msg) => {
                write!(f, "{}", msg)
            }
        }
    }
}
