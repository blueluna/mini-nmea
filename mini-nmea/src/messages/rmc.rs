use super::{FromSentence, ModeIndicator, Status};
use crate::{
    parse::{
        parse_direction_field, parse_hmsf, parse_latitude, parse_longitude, parse_number_maybe,
        parse_ymd, FieldIterator,
    },
    Error, MessageId, Sentence,
};
use chrono::{DateTime, TimeZone, Utc};
use core::convert::TryFrom;

/// # RMC, Recommended Minimum Specific GPS Data
pub struct Rmc {
    /// UTC time of fix
    pub datetime: Option<DateTime<Utc>>,
    /// Status
    pub status: Option<Status>,
    /// Latitude in degrees
    pub latitude: Option<f64>,
    /// Longitude in degrees
    pub longitude: Option<f64>,
    /// Speed over ground, in Knots
    pub speed_over_ground: Option<f64>,
    /// Track made good, in degrees
    pub track_made_good: Option<f64>,
    /// Magnetic variation
    pub magnetic_variation: Option<f64>,
    /// Mode indicator
    pub mode_indicator: Option<ModeIndicator>,
}

impl FromSentence for Rmc {
    fn from_sentence(sentence: &Sentence) -> Result<Self, Error> {
        if sentence.message_id != Some(MessageId::Rmc) {
            return Err(Error::InvalidMessageIdentifier);
        }
        let mut iterator = FieldIterator::from_bytes(sentence.data);
        let time = parse_hmsf(iterator.next_err()?).ok();
        let status = Status::try_from(iterator.next_err()?).ok();
        let latitude = parse_latitude(iterator.next_err()?, iterator.next_err()?).ok();
        let longitude = parse_longitude(iterator.next_err()?, iterator.next_err()?).ok();
        let speed_over_ground = parse_number_maybe::<f64>(iterator.next_err()?)?;
        let track_made_good = parse_number_maybe::<f64>(iterator.next_err()?)?;
        let date = parse_ymd(iterator.next_err()?).ok();
        let magnetic_variation = parse_number_maybe::<f64>(iterator.next_err()?)?;
        let mv_dir = parse_direction_field(iterator.next_err()?, false).ok();
        let magnetic_variation = match (magnetic_variation, mv_dir) {
            (Some(deg), Some(dir)) => Some(deg * (-1.0 * dir)),
            (_, _) => None,
        };
        // Mode indication might not exist
        let mode_indicator = match iterator.next() {
            Some(bytes) => ModeIndicator::try_from(bytes).ok(),
            None => None,
        };

        let datetime = match (date, time) {
            (Some(date), Some(time)) => date.and_time(time),
            (_, _) => None,
        };

        Ok(Self {
            datetime,
            status,
            latitude,
            longitude,
            speed_over_ground,
            track_made_good,
            magnetic_variation,
            mode_indicator,
        })
    }
}

impl core::fmt::Display for Rmc {
    fn fmt(&self, f: &mut core::fmt::Formatter) -> core::fmt::Result {
        write!(
            f,
            "Rmc {} {} {}",
            self.datetime
                .unwrap_or_else(|| Utc.ymd(1900, 1, 1).and_hms(0, 0, 0)),
            self.longitude.unwrap_or(99.999),
            self.latitude.unwrap_or(999.999)
        )
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use chrono::TimeZone;

    #[test]
    fn parse_ok() {
        let (sentence, _) = Sentence::parse(
            "$GPRMC,161229.487,A,3723.2475,N,12158.3416,W,0.13,309.62,120598,,*10 ".as_bytes(),
        )
        .unwrap();
        let rmc = Rmc::from_sentence(&sentence).unwrap();
        assert_eq!(
            rmc.datetime,
            Some(Utc.ymd(1998, 05, 12).and_hms_micro(16, 12, 29, 487000))
        );
        assert_eq!(rmc.status, Some(Status::Ok));
        assert_eq!(rmc.latitude, Some(37.387458333333335));
        assert_eq!(rmc.longitude, Some(-121.97236));
        assert_eq!(rmc.speed_over_ground, Some(0.13));
        assert_eq!(rmc.track_made_good, Some(309.62));
        assert_eq!(rmc.magnetic_variation, None);
        assert_eq!(rmc.mode_indicator, None);

        let (sentence, _) = Sentence::parse(
            "$GPRMC,074529.82,A,2429.6717,N,11804.6973,E,12.623,32.122,010806,,W,A*08".as_bytes(),
        )
        .unwrap();
        let rmc = Rmc::from_sentence(&sentence).unwrap();
        assert_eq!(
            rmc.datetime,
            Some(Utc.ymd(2006, 08, 01).and_hms_micro(07, 45, 29, 820000))
        );
        assert_eq!(rmc.status, Some(Status::Ok));
        assert_eq!(rmc.latitude, Some(24.494528333333335));
        assert_eq!(rmc.longitude, Some(118.07828833333333));
        assert_eq!(rmc.speed_over_ground, Some(12.623));
        assert_eq!(rmc.track_made_good, Some(32.122));
        assert_eq!(rmc.magnetic_variation, None);
        assert_eq!(rmc.mode_indicator, Some(ModeIndicator::Autonomous));
    }
}
