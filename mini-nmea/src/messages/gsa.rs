use super::FromSentence;
use crate::{
    character_field,
    parse::{parse_number_maybe, FieldIterator},
    Error, MessageId, Sentence,
};
use core::convert::TryFrom;

character_field!(
    /// Operation Mode
    enum OperationMode {
        /// Automatic mode
        b'A' = Automatic,
        /// Manual mode
        b'M' = Manual,
    }
);

impl core::fmt::Display for OperationMode {
    fn fmt(&self, f: &mut core::fmt::Formatter) -> core::fmt::Result {
        match self {
            Self::Automatic => write!(f, "Automatic"),
            Self::Manual => write!(f, "Manual"),
        }
    }
}

character_field!(
    /// Fix Mode
    enum FixMode {
        /// No Fix
        b'1' = NoFix,
        /// 2D Fix
        b'2' = Fix2d,
        /// 3D Fix
        b'3' = Fix3d,
    }
);

impl core::fmt::Display for FixMode {
    fn fmt(&self, f: &mut core::fmt::Formatter) -> core::fmt::Result {
        match self {
            Self::NoFix => write!(f, "No Fix"),
            Self::Fix2d => write!(f, "2D Fix"),
            Self::Fix3d => write!(f, "3D Fix"),
        }
    }
}

/// # GSA,
#[derive(Clone, Debug, PartialEq)]
pub struct Gsa {
    /// Operation Mode
    pub operation_mode: OperationMode,
    /// Fix Mode
    pub fix_mode: FixMode,
    /// Pseudo Random Noise (PRN) Code for up to 12 satellites
    prn_codes: [Option<u8>; 12],
    /// Position dilution of precision
    position_dop: Option<f64>,
    /// Horizontal dilution of precision
    horizontal_dop: Option<f64>,
    /// Vertical dilution of precision
    vertical_dop: Option<f64>,
    /// GNSS system identity
    system_id: Option<u8>,
}

impl FromSentence for Gsa {
    fn from_sentence(sentence: &Sentence) -> Result<Self, Error> {
        if sentence.message_id != Some(MessageId::Gsa) {
            return Err(Error::InvalidMessageIdentifier);
        }
        let mut iterator = FieldIterator::from_bytes(sentence.data);
        let operation_mode = OperationMode::try_from(iterator.next_err()?)?;
        let fix_mode = FixMode::try_from(iterator.next_err()?)?;
        let mut prn_codes = [None; 12];
        for code in &mut prn_codes {
            *code = parse_number_maybe::<u8>(iterator.next_err()?)?;
        }
        let position_dop = parse_number_maybe::<f64>(iterator.next_err()?)?;
        let horizontal_dop = parse_number_maybe::<f64>(iterator.next_err()?)?;
        let vertical_dop = parse_number_maybe::<f64>(iterator.next_err()?)?;
        // System id might not exist
        let system_id = match iterator.next() {
            Some(bytes) => parse_number_maybe::<u8>(bytes)?,
            None => None,
        };

        Ok(Self {
            operation_mode,
            fix_mode,
            prn_codes,
            position_dop,
            horizontal_dop,
            vertical_dop,
            system_id,
        })
    }
}

impl core::fmt::Display for Gsa {
    fn fmt(&self, f: &mut core::fmt::Formatter) -> core::fmt::Result {
        write!(
            f,
            "Gsa {} {} PDOP {:5.2} HDOP {:5.2} VDOP {:5.2}",
            self.operation_mode,
            self.fix_mode,
            self.position_dop.unwrap_or(0.0),
            self.horizontal_dop.unwrap_or(0.0),
            self.vertical_dop.unwrap_or(0.0),
        )
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn parse_ok() {
        let (sentence, _) =
            Sentence::parse("$GPGSA,M,3,17,02,30,04,05,10,09,06,31,12,,,1.2,0.8,0.9*35".as_bytes())
                .unwrap();
        let msg = Gsa::from_sentence(&sentence).unwrap();
        assert_eq!(msg.operation_mode, OperationMode::Manual);
        assert_eq!(msg.fix_mode, FixMode::Fix3d);
        assert_eq!(
            msg.prn_codes,
            [
                Some(17),
                Some(2),
                Some(30),
                Some(4),
                Some(5),
                Some(10),
                Some(9),
                Some(6),
                Some(31),
                Some(12),
                None,
                None
            ]
        );
        assert_eq!(msg.position_dop, Some(1.2));
        assert_eq!(msg.horizontal_dop, Some(0.8));
        assert_eq!(msg.vertical_dop, Some(0.9));
        let (sentence, _) =
            Sentence::parse("$GNGSA,M,3,87,70,,,,,,,,,,,1.2,0.8,0.9*2A".as_bytes()).unwrap();
        let msg = Gsa::from_sentence(&sentence).unwrap();
        assert_eq!(msg.operation_mode, OperationMode::Manual);
        assert_eq!(msg.fix_mode, FixMode::Fix3d);
        assert_eq!(
            msg.prn_codes,
            [
                Some(87),
                Some(70),
                None,
                None,
                None,
                None,
                None,
                None,
                None,
                None,
                None,
                None
            ]
        );
        assert_eq!(msg.position_dop, Some(1.2));
        assert_eq!(msg.horizontal_dop, Some(0.8));
        assert_eq!(msg.vertical_dop, Some(0.9));
    }
}
