use crate::{Error, Sentence};

/// From `Sentence` trait
pub trait FromSentence: Sized {
    /// Create from sentence
    fn from_sentence(sentence: &Sentence) -> Result<Self, Error>;
}
