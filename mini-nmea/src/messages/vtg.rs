use super::{FromSentence, ModeIndicator, NauticalSpeed, Speed};
use crate::{
    character_field,
    parse::{parse_number_maybe, FieldIterator},
    Error, MessageId, Sentence,
};
use core::convert::TryFrom;

character_field!(
    /// True Track
    enum TrueTrack {
        /// True Track
        b'T' = TrueTrack,
    }
);

character_field!(
    /// Magnetic Track
    enum MagneticTrack {
        /// Magnetic Track
        b'M' = MagneticTrack,
    }
);

/// # VTG,
#[derive(Clone, Debug, PartialEq)]
pub struct Vtg {
    /// Track made good, degrees true
    true_track: Option<f64>,
    /// Track made good, degrees magnetic
    magnetic_track: Option<f64>,
    /// Speed over ground in Knots
    speed_over_ground: Option<f64>,
    /// Speed in Kilometers per hour
    speed: Option<f64>,
    /// Positioning system mode indicator
    mode_indicator: Option<ModeIndicator>,
}

impl FromSentence for Vtg {
    fn from_sentence(sentence: &Sentence) -> Result<Self, Error> {
        if sentence.message_id != Some(MessageId::Vtg) {
            return Err(Error::InvalidMessageIdentifier);
        }
        let mut iterator = FieldIterator::from_bytes(sentence.data);
        let true_track = parse_number_maybe::<f64>(iterator.next_err()?)?;
        let _ = TrueTrack::try_from_maybe(iterator.next_err()?)?;
        let magnetic_track = parse_number_maybe::<f64>(iterator.next_err()?)?;
        let _ = MagneticTrack::try_from_maybe(iterator.next_err()?)?;
        let speed_over_ground = parse_number_maybe::<f64>(iterator.next_err()?)?;
        let _ = NauticalSpeed::try_from_maybe(iterator.next_err()?)?;
        let speed = parse_number_maybe::<f64>(iterator.next_err()?)?;
        let _ = Speed::try_from_maybe(iterator.next_err()?)?;
        // Mode indication might not exist
        let mode_indicator = match iterator.next() {
            Some(bytes) => ModeIndicator::try_from_maybe(bytes)?,
            None => None,
        };
        Ok(Self {
            true_track,
            magnetic_track,
            speed_over_ground,
            speed,
            mode_indicator,
        })
    }
}

impl core::fmt::Display for Vtg {
    fn fmt(&self, f: &mut core::fmt::Formatter) -> core::fmt::Result {
        write!(
            f,
            "Vtg True {:6.2} Magnetic {:6.2} SOG {:5.2} S {:5.2} Mode {}",
            self.true_track.unwrap_or(0.0),
            self.magnetic_track.unwrap_or(0.0),
            self.speed_over_ground.unwrap_or(0.0),
            self.speed.unwrap_or(0.0),
            self.mode_indicator.unwrap_or(ModeIndicator::NotValid),
        )
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn parse_ok() {
        let (sentence, _) =
            Sentence::parse("$GPVTG,172.516,T,155.295,M,0.049,N,0.090,K,D*2B".as_bytes()).unwrap();
        let msg = Vtg::from_sentence(&sentence).unwrap();
        assert_eq!(msg.true_track, Some(172.516));
        assert_eq!(msg.magnetic_track, Some(155.295));
        assert_eq!(msg.speed_over_ground, Some(0.049));
        assert_eq!(msg.speed, Some(0.090));
        assert_eq!(msg.mode_indicator, Some(ModeIndicator::Differential));
        let (sentence, _) =
            Sentence::parse("$GNVTG,134.395,T,134.395,M,0.019,N,0.035,K,A*33".as_bytes()).unwrap();
        let msg = Vtg::from_sentence(&sentence).unwrap();
        assert_eq!(msg.true_track, Some(134.395));
        assert_eq!(msg.magnetic_track, Some(134.395));
        assert_eq!(msg.speed_over_ground, Some(0.019));
        assert_eq!(msg.speed, Some(0.035));
        assert_eq!(msg.mode_indicator, Some(ModeIndicator::Autonomous));

        let (sentence, _) = Sentence::parse("$GNVTG,,,,,,,,,N*2E".as_bytes()).unwrap();
        let msg = Vtg::from_sentence(&sentence).unwrap();
        assert_eq!(msg.true_track, None);
        assert_eq!(msg.magnetic_track, None);
        assert_eq!(msg.speed_over_ground, None);
        assert_eq!(msg.speed, None);
        assert_eq!(msg.mode_indicator, Some(ModeIndicator::NotValid));
    }
}
