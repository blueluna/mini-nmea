use super::FromSentence;
use crate::{
    parse::{parse_hmsf, parse_number_maybe, FieldIterator},
    Error, MessageId, Sentence,
};
use chrono::NaiveTime;

/// # GST,
#[derive(Clone, Debug, PartialEq)]
pub struct Gst {
    /// UTC time of fix
    pub time: Option<NaiveTime>,
    /// RMS value of the pseudorange residuals; includes carrier phase residuals during periods of RTK (float) and RTK (fixed) processing
    rms_value: Option<f64>,
    /// Error ellipse semi-major axis sigma error, in meters
    error_ellipse_major: Option<f64>,
    /// Error ellipse semi-minor axis sigma error, in meters
    error_ellipse_minor: Option<f64>,
    /// Error ellipse orientation, degrees from true north
    error_ellipse_azimuth: Option<f64>,
    /// Latitude sigma error, in meters
    latitude_sigma: Option<f64>,
    /// Longitude sigma error, in meters
    longitude_sigma: Option<f64>,
    /// Height sigma error, in meters
    height_sigma: Option<f64>,
}

impl FromSentence for Gst {
    fn from_sentence(sentence: &Sentence) -> Result<Self, Error> {
        if sentence.message_id != Some(MessageId::Gst) {
            return Err(Error::InvalidMessageIdentifier);
        }
        let mut iterator = FieldIterator::from_bytes(sentence.data);
        let time = parse_hmsf(iterator.next_err()?).ok();
        let rms_value = parse_number_maybe::<f64>(iterator.next_err()?)?;
        let error_ellipse_major = parse_number_maybe::<f64>(iterator.next_err()?)?;
        let error_ellipse_minor = parse_number_maybe::<f64>(iterator.next_err()?)?;
        let error_ellipse_azimuth = parse_number_maybe::<f64>(iterator.next_err()?)?;
        let latitude_sigma = parse_number_maybe::<f64>(iterator.next_err()?)?;
        let longitude_sigma = parse_number_maybe::<f64>(iterator.next_err()?)?;
        let height_sigma = parse_number_maybe::<f64>(iterator.next_err()?)?;
        Ok(Self {
            time,
            rms_value,
            error_ellipse_major,
            error_ellipse_minor,
            error_ellipse_azimuth,
            latitude_sigma,
            longitude_sigma,
            height_sigma,
        })
    }
}

impl core::fmt::Display for Gst {
    fn fmt(&self, f: &mut core::fmt::Formatter) -> core::fmt::Result {
        write!(
            f,
            "Gst Latitude {:6.2} Longitude {:6.2} Height {:5.2}",
            self.latitude_sigma.unwrap_or(0.0),
            self.longitude_sigma.unwrap_or(0.0),
            self.height_sigma.unwrap_or(0.0),
        )
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn parse_ok() {
        let (sentence, _) = Sentence::parse(
            "$GPGST,172814.0,0.006,0.023,0.020,273.6,0.023,0.020,0.031*6A".as_bytes(),
        )
        .unwrap();
        let msg = Gst::from_sentence(&sentence).unwrap();
        assert_eq!(msg.time, Some(NaiveTime::from_hms(17, 28, 14)));
        assert_eq!(msg.rms_value, Some(0.006));
        assert_eq!(msg.error_ellipse_major, Some(0.023));
        assert_eq!(msg.error_ellipse_minor, Some(0.020));
        assert_eq!(msg.error_ellipse_azimuth, Some(273.6));
        assert_eq!(msg.latitude_sigma, Some(0.023));
        assert_eq!(msg.longitude_sigma, Some(0.020));
        assert_eq!(msg.height_sigma, Some(0.031));
    }
}
