use super::FromSentence;
use crate::{
    parse::{parse_number, parse_number_maybe, FieldIterator},
    Error, MessageId, Sentence,
};

/// Satelite information
#[derive(Clone, Debug, PartialEq)]
pub struct Satellite {
    /// Pseudo Random Noise (PRN) code
    pub pseudo_random_noise_code: u8,
    /// Elevation in meters
    pub elevation: f64,
    /// Azimuth angle from north?
    pub azimuth: f64,
    /// Signal to noise ratio (SNR)
    pub signal_to_noise_ratio: Option<u8>,
}

impl core::default::Default for Satellite {
    fn default() -> Self {
        Self {
            pseudo_random_noise_code: 0,
            elevation: 0.0,
            azimuth: 0.0,
            signal_to_noise_ratio: None,
        }
    }
}

/// # GSV,
#[derive(Clone, Debug, PartialEq)]
pub struct Gsv {
    /// Message count
    pub message_count: u8,
    /// Message index
    pub message_index: u8,
    /// Total number of satellites
    pub satellite_count: u8,
    /// Satellite information
    pub satellites: [Satellite; 4],
}

impl FromSentence for Gsv {
    fn from_sentence(sentence: &Sentence) -> Result<Self, Error> {
        if sentence.message_id != Some(MessageId::Gsv) {
            return Err(Error::InvalidMessageIdentifier);
        }
        let mut iterator = FieldIterator::from_bytes(sentence.data);
        let message_count = parse_number::<u8>(iterator.next_err()?)?;
        let message_index = parse_number::<u8>(iterator.next_err()?)?;
        let satellite_count = parse_number::<u8>(iterator.next_err()?)?;
        let mut satellites = [
            Satellite::default(),
            Satellite::default(),
            Satellite::default(),
            Satellite::default(),
        ];
        for satellite in &mut satellites {
            // Check if there are more data
            let prn_code = match iterator.next() {
                Some(bytes) => parse_number::<u8>(bytes)?,
                None => break,
            };
            (*satellite).pseudo_random_noise_code = prn_code;
            (*satellite).elevation = parse_number::<f64>(iterator.next_err()?)?;
            (*satellite).azimuth = parse_number::<f64>(iterator.next_err()?)?;
            (*satellite).signal_to_noise_ratio = parse_number_maybe::<u8>(iterator.next_err()?)?;
        }
        Ok(Self {
            message_count,
            message_index,
            satellite_count,
            satellites,
        })
    }
}

impl core::fmt::Display for Gsv {
    fn fmt(&self, f: &mut core::fmt::Formatter) -> core::fmt::Result {
        write!(
            f,
            "Gsv {} {} {} 1 {:3} 2 {:3} 3 {:3} 4 {:3}",
            self.message_count,
            self.message_index,
            self.satellite_count,
            self.satellites[0].pseudo_random_noise_code,
            self.satellites[1].pseudo_random_noise_code,
            self.satellites[2].pseudo_random_noise_code,
            self.satellites[3].pseudo_random_noise_code,
        )
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn parse_ok() {
        let (sentence, _) = Sentence::parse(
            "$GPGSV,4,1,13,02,02,213,,03,-3,000,,11,00,121,,14,13,172,05*67".as_bytes(),
        )
        .unwrap();
        let msg = Gsv::from_sentence(&sentence).unwrap();
        assert_eq!(msg.message_count, 4);
        assert_eq!(msg.message_index, 1);
        assert_eq!(msg.satellite_count, 13);
        assert_eq!(msg.satellites[0].pseudo_random_noise_code, 2);
        assert_eq!(msg.satellites[0].elevation, 2.0);
        assert_eq!(msg.satellites[0].azimuth, 213.0);
        assert_eq!(msg.satellites[0].signal_to_noise_ratio, None);
        assert_eq!(msg.satellites[1].pseudo_random_noise_code, 3);
        assert_eq!(msg.satellites[1].elevation, -3.0);
        assert_eq!(msg.satellites[1].azimuth, 0.0);
        assert_eq!(msg.satellites[1].signal_to_noise_ratio, None);
        assert_eq!(msg.satellites[2].pseudo_random_noise_code, 11);
        assert_eq!(msg.satellites[2].elevation, 0.0);
        assert_eq!(msg.satellites[2].azimuth, 121.0);
        assert_eq!(msg.satellites[2].signal_to_noise_ratio, None);
        assert_eq!(msg.satellites[3].pseudo_random_noise_code, 14);
        assert_eq!(msg.satellites[3].elevation, 13.0);
        assert_eq!(msg.satellites[3].azimuth, 172.0);
        assert_eq!(msg.satellites[3].signal_to_noise_ratio, Some(5));
    }
}
