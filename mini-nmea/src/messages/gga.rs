use super::{FromSentence, GnssQualityIndicator};
use crate::{
    parse::{parse_hmsf, parse_latitude, parse_longitude, parse_number_maybe, FieldIterator},
    Error, MessageId, Sentence,
};
use chrono::NaiveTime;

/// # GGA, GPS Fix Data
pub struct Gga {
    /// UTC time of fix
    pub time: Option<NaiveTime>,
    /// Latitude in degrees
    pub latitude: Option<f64>,
    /// Longitude in degrees
    pub longitude: Option<f64>,
    /// GNSS quality indicator
    pub quality: Option<GnssQualityIndicator>,
    /// Number of satellites in use
    pub satellites: Option<u8>,
    /// Horizontal dilution of position
    pub hdop: Option<f64>,
    /// Altitude above mean sea level in meters
    pub altitude: Option<f64>,
    /// Height of geoid (mean sea level) above WGS84 ellipsoid
    pub geoid_separation: Option<f64>,
    /// Age of differential GNSS data record
    pub dgps_age: Option<f64>,
    /// Reference station ID
    pub reference_station_id: Option<u16>,
}

impl FromSentence for Gga {
    fn from_sentence(sentence: &Sentence) -> Result<Self, Error> {
        if sentence.message_id != Some(MessageId::Gga) {
            return Err(Error::InvalidMessageIdentifier);
        }
        let mut iterator = FieldIterator::from_bytes(sentence.data);
        let time = parse_hmsf(iterator.next_err()?).ok();
        let latitude = parse_latitude(iterator.next_err()?, iterator.next_err()?).ok();
        let longitude = parse_longitude(iterator.next_err()?, iterator.next_err()?).ok();
        let quality = GnssQualityIndicator::try_from_maybe(iterator.next_err()?)?;
        let satellites = parse_number_maybe::<u8>(iterator.next_err()?)?;
        let hdop = parse_number_maybe::<f64>(iterator.next_err()?)?;
        let altitude = parse_number_maybe::<f64>(iterator.next_err()?)?;
        match iterator.next_err()? {
            b"M" => (),
            b"" => (),
            _ => {
                return Err(Error::InvalidValue);
            }
        }
        let geoid_separation = parse_number_maybe::<f64>(iterator.next_err()?)?;
        match iterator.next_err()? {
            b"M" => (),
            b"" => (),
            _ => {
                return Err(Error::InvalidValue);
            }
        }
        let dgps_age = parse_number_maybe::<f64>(iterator.next_err()?)?;
        let reference_station_id = parse_number_maybe::<u16>(iterator.next_err()?)?;
        Ok(Self {
            time,
            latitude,
            longitude,
            quality,
            satellites,
            hdop,
            altitude,
            geoid_separation,
            dgps_age,
            reference_station_id,
        })
    }
}

impl core::fmt::Display for Gga {
    fn fmt(&self, f: &mut core::fmt::Formatter) -> core::fmt::Result {
        write!(
            f,
            "Gga {} {} {}",
            self.time.unwrap_or_else(|| NaiveTime::from_hms(0, 0, 0)),
            self.longitude.unwrap_or(99.999),
            self.latitude.unwrap_or(999.999)
        )
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn parse_ok() {
        let (sentence, _) = Sentence::parse(
            "$GNGGA,185824.00,2605.0247881,N,08014.3097082,W,4,14,0.7,-24.343,M,,,0.59,0402*36"
                .as_bytes(),
        )
        .unwrap();
        let gga = Gga::from_sentence(&sentence).unwrap();
        assert_eq!(gga.time, Some(NaiveTime::from_hms(18, 58, 24)));
        assert_eq!(gga.latitude, Some(26.083746468333334));
        assert_eq!(gga.longitude, Some(-80.23849513666667));
        assert_eq!(gga.quality, Some(GnssQualityIndicator::RealTimeKinematic));
        assert_eq!(gga.satellites, Some(14));
        assert_eq!(gga.hdop, Some(0.7));
        assert_eq!(gga.altitude, Some(-24.343));
        assert_eq!(gga.geoid_separation, None);
        assert_eq!(gga.dgps_age, Some(0.59));
        assert_eq!(gga.reference_station_id, Some(402));
    }
}
