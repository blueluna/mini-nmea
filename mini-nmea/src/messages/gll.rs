use super::{FromSentence, ModeIndicator, Status};
use crate::{
    parse::{parse_hmsf, parse_latitude, parse_longitude, FieldIterator},
    Error, MessageId, Sentence,
};
use chrono::NaiveTime;
use core::convert::TryFrom;

/// # GLL, Position Data
pub struct Gll {
    /// UTC time of fix
    pub time: Option<NaiveTime>,
    /// Latitude in degrees
    pub latitude: Option<f64>,
    /// Longitude in degrees
    pub longitude: Option<f64>,
    /// GNSS status indicator
    pub status: Option<Status>,
    /// Positioning system mode indicator
    pub mode_indicator: Option<ModeIndicator>,
}

impl FromSentence for Gll {
    fn from_sentence(sentence: &Sentence) -> Result<Self, Error> {
        if sentence.message_id != Some(MessageId::Gll) {
            return Err(Error::InvalidMessageIdentifier);
        }
        let mut iterator = FieldIterator::from_bytes(sentence.data);
        let latitude = parse_latitude(iterator.next_err()?, iterator.next_err()?).ok();
        let longitude = parse_longitude(iterator.next_err()?, iterator.next_err()?).ok();
        let time = parse_hmsf(iterator.next_err()?).ok();
        let status = Status::try_from(iterator.next_err()?).ok();
        // Mode indication might not exist
        let mode_indicator = match iterator.next() {
            Some(bytes) => ModeIndicator::try_from(bytes).ok(),
            None => None,
        };
        Ok(Self {
            time,
            latitude,
            longitude,
            status,
            mode_indicator,
        })
    }
}

impl core::fmt::Display for Gll {
    fn fmt(&self, f: &mut core::fmt::Formatter) -> core::fmt::Result {
        write!(
            f,
            "Gll {} {} {}",
            self.time.unwrap_or_else(|| NaiveTime::from_hms(0, 0, 0)),
            self.longitude.unwrap_or(99.999),
            self.latitude.unwrap_or(999.999)
        )
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn parse_ok() {
        let (sentence, _) =
            Sentence::parse("$GNGLL,5107.0014143,N,11402.3278489,W,205122.00,A,A*6E".as_bytes())
                .unwrap();
        let msg = Gll::from_sentence(&sentence).unwrap();
        assert_eq!(msg.time, Some(NaiveTime::from_hms(20, 51, 22)));
        assert_eq!(msg.latitude, Some(51.116690238333334));
        assert_eq!(msg.longitude, Some(-114.03879748166666));
        assert_eq!(msg.status, Some(Status::Ok));
        assert_eq!(msg.mode_indicator, Some(ModeIndicator::Autonomous));
    }
}
