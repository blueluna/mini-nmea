use crate::Error;
use core::convert::TryFrom;

/// Generate enumeration using single character field
#[macro_export]
macro_rules! character_field {
    (
        $(#[$outer:meta])*
        enum $Name:ident {
            $(
                #[$inner:meta]
                $ItemCharacter:literal = $ItemName:ident
            ),*
            $(,)*
        }
    ) => {
        $(#[$outer])*
        #[derive(Clone, Copy, Debug, PartialEq)]
        pub enum $Name {
            $(#[$inner] $ItemName),*,
        }

        impl TryFrom<u8> for $Name {
            type Error = Error;
            fn try_from(value: u8) -> Result<Self, Error> {
                let this = match value {
                    $($ItemCharacter => Self::$ItemName,)*
                    _ => return Err(Error::InvalidValue),
                };
                Ok(this)
            }
        }

        impl TryFrom<&[u8]> for $Name {
            type Error = Error;
            fn try_from(value: &[u8]) -> Result<Self, Error> {
                if value.len() == 1 {
                    Self::try_from(value[0])
                }
                else if value.len() == 0 {
                    Err(Error::EmptyField)
                }
                else {
                    Err(Error::InvalidLength)
                }
            }
        }

        impl $Name {
            /// Try to decode character.
            /// If the character is decoded the value is returned as Some(T).
            /// If the field is empty None is returned.
            /// If an error occurred Err(Error) is returned.
            pub fn try_from_maybe(value: &[u8]) -> Result<Option<Self>, Error> {
                match Self::try_from(value) {
                    Ok(v) => Ok(Some(v)),
                    Err(Error::EmptyField) => Ok(None),
                    Err(error) => Err(error),
                }
            }
        }
    }
}

character_field!(
    /// Status
    enum Status {
        /// Status OK
        b'A' = Ok,
        /// Warning
        b'V' = Warning,
    }
);

character_field!(
    /// Positioning system mode indicator
    enum ModeIndicator {
        /// Autonomous
        b'A' = Autonomous,
        /// Differential
        b'D' = Differential,
        /// Estimated
        b'E' = Estimated,
        /// Manual
        b'M' = Manual,
        /// Not valid
        b'N' = NotValid,
    }
);

impl core::fmt::Display for ModeIndicator {
    fn fmt(&self, f: &mut core::fmt::Formatter) -> core::fmt::Result {
        match self {
            Self::Autonomous => write!(f, "Autonomous"),
            Self::Differential => write!(f, "Differential"),
            Self::Estimated => write!(f, "Estimated"),
            Self::Manual => write!(f, "Manual"),
            Self::NotValid => write!(f, "Not Valid"),
        }
    }
}

character_field!(
    /// GNSS quality indicator
    enum GnssQualityIndicator {
        /// No position fix
        b'0' = NoFix,
        /// GNSS fix
        b'1' = GnssFix,
        /// DGPS fix
        b'2' = DGpsFix,
        /// PPS fix
        b'3' = PpsFix,
        /// Real-time kinematic (RTK) fix
        b'4' = RealTimeKinematic,
        /// Real-time kinematic (RTK) fix (float?)
        b'5' = RealTimeKinematicFloat,
        /// Dead reckoning
        b'6' = DeadReckoning,
        /// Manual input mode
        b'7' = ManualInputMode,
        /// Simulation mode
        b'8' = SimulationMode,
    }
);

character_field!(
    /// Speed
    enum Speed {
        /// Kilometers per hour
        b'K' = Kph,
    }
);

character_field!(
    /// Nautical speed
    enum NauticalSpeed {
        /// Knots
        b'N' = Knots,
    }
);
