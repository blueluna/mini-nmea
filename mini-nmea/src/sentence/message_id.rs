use crate::Error;
use core::convert::TryFrom;
use mini_nmea_macro::FieldUpperCase;

/// Message identifier
#[derive(Clone, Copy, Debug, FieldUpperCase, PartialEq)]
pub enum MessageId {
    /// Waypoint Arrival Alarm
    Aam,
    /// AIS Addressed and Binary Broadcast Acknowledgement
    Abk,
    /// AIS Addressed and Binary and Safety Related Message
    Abm,
    /// AIS Regional Channel Assignment Message
    Aca,
    /// Acknowledge Alarm
    Ack,
    /// AIS Channel Management Information Source
    Acs,
    /// AIS Interrogation Request
    Air,
    /// Almanac Data
    Alm,
    /// Set Alarm State
    Alr,
    /// Auto Pilot, A
    Apa,
    /// Auto Pilot, B
    Apb,
    /// AIS Base Station Broadcast of Assignment Command
    Asn,
    /// Bearing & Distance to Waypoint - Dead Reckoning
    Bec,
    /// Bearing, Origin to Destination
    Bod,
    /// Bearing & Distance to Waypoint - Great Circle
    Bwc,
    /// Bearing & Distance to Waypoint - Rhumb Line
    Bwr,
    /// Bearing Waypoint to Waypoint
    Bww,
    /// Water Current
    Cur,
    /// Depth Below Transducer
    Dbt,
    /// Decca position
    Dcn,
    /// Depth
    Dpt,
    /// Digital Selective Calling Information
    Dsc,
    /// Expanded Digital Selective Calling Information
    Dse,
    /// DSC Transponder Initialize
    Dsi,
    /// DSC Transponder Response
    Dsr,
    /// Datum Reference
    Dtm,
    /// Frequency Set Status or Command
    Fsi,
    /// GNSS Satellite Fault Detection
    Gbs,
    /// Fix Information
    Gga,
    /// Geographic Position - Loran-C
    Glc,
    /// Geographic Position - Latitude / Longitude
    Gll,
    /// GNSS Map Projection Fix Data
    Gmp,
    /// GNSS Fix Data
    Gns,
    /// GNSS Range Residuals
    Grs,
    /// GNSS DOP and Active Satellites
    Gsa,
    /// GNSS Pseudorange Error Statistics
    Gst,
    /// GNSS Satellites In View
    Gsv,
    /// Heading. Deviation & Variation
    Hdg,
    /// Heading. True.
    Hdt,
    /// Heading Monitor Receive
    Hmr,
    /// Heading Monitor Set
    Hms,
    /// Heading Steering Command
    Hsc,
    /// Heading / Track Control Command
    Htc,
    /// Heading / Track Control Data
    Htd,
    /// Loran-C Signal Data
    Lcd,
    /// AIS Long-Range Function
    Lrf,
    /// AIS Long-Range Interrogation
    Lri,
    /// AIS Long-Range Reply Sentence 1
    Lr1,
    /// AIS Long-Range Reply Sentence 2
    Lr2,
    /// AIS Long-Range Reply Sentence 3
    Lr3,
    /// GLONASS Almanac
    Mla,
    /// MSK Receiver Interface Command
    Msk,
    /// MSK Receiver Signal
    Mss,
    /// Wind Direction & Speed
    Mwd,
    /// Water Temperature
    Mtw,
    /// Wind Speed & Angle
    Mwv,
    /// Own Ship Data
    Osd,
    /// Recommended Minimum Specific Loran-C Data
    Rma,
    /// Recommended Minimum Navigation Information
    Rmb,
    /// Recommended Minimum Specific GNSS Data
    Rmc,
    /// Rate Of Turn
    Rot,
    /// Revolutions
    Rpm,
    /// Rudder Sensor Angle
    Rsa,
    /// Radar System Data
    Rsd,
    /// Routes
    Rte,
    /// Scanning Frequency Information Status and Command
    Sfi,
    /// AIS Ship Static Data
    Ssd,
    /// Multiple Data Identifier
    Stn,
    /// Target Label
    Tlb,
    /// Target Latitude & Longitude
    Tll,
    /// Transit Fix Data
    Trf,
    /// Tracked Target Message
    Ttm,
    /// Transmission of Multi-language text
    Tut,
    /// Text Transmission
    Txt,
    /// Dual Ground / Water Speed
    Vbw,
    /// AIS VHF Data-Link Message
    Vdm,
    /// AIS VHF Data-Link Own-Vessel Report
    Vdo,
    /// Set & Drift
    Vdr,
    /// Water Speed and Heading
    Vhw,
    /// Dual Ground / Water Distance
    Vlw,
    /// Speed - Measured Parallel to Wind
    Vpw,
    /// AIS Voyage Static Data
    Vsd,
    /// Course Iver Ground & Ground Speed
    Vtg,
    /// Water Level Detection
    Wat,
    /// Waypoint Closure Velocity
    Wcv,
    /// Distance - Waypoint to Waypoint
    Wnc,
    /// Waypoint Location
    Wpl,
    /// Transducer Measurements
    Xdr,
    /// Cross-Track Error, Measured
    Xte,
    /// Cross-Track Error, Dead Reckoning
    Xtr,
    /// Time & Date
    Zda,
    /// Time & Distance to Variable Point
    Zdl,
    /// UTC & Time from Origin Waypoint
    Zfo,
    /// UTC & Time to Destination Waypoint
    Ztg,
}

impl MessageId {
    /// Parse bytes into `MessageId`
    pub fn from_bytes(data: &[u8]) -> Result<(MessageId, usize), Error> {
        if data.len() < 3 {
            return Err(Error::InvalidLength);
        }
        let message_id = MessageId::try_from(&data[..3])?;
        Ok((message_id, 3))
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn from_bytes() {
        assert_eq!(
            MessageId::Gga,
            MessageId::try_from("GGA".as_bytes()).unwrap()
        );
        assert_eq!(
            Err(Error::InvalidValue),
            MessageId::try_from("XXX".as_bytes())
        );
    }
}
