use crate::Error;
use core::convert::TryFrom;
use mini_nmea_macro::FieldUpperCase;

macro_rules! manufacturer_id_enum {
    (
        $(#[$outer:meta])*
        enum $Name:ident { $($Variant:ident = $VariantName:literal),* $(,)* }
    ) => {
        $(#[$outer])*
        #[derive(Clone, Copy, Debug, FieldUpperCase, PartialEq)]
        pub enum $Name {
            $($Variant),*,
        }

        impl $Name {
            #[doc = "Manufacturer name"]
            pub fn name(&self) -> &'static str {
                match *self {
                    $($Name::$Variant => $VariantName,)*
                }
            }
            #[doc = "Parse bytes into ManufacturerId"]
            pub fn from_bytes(data: &[u8]) -> Result<($Name, usize), Error> {
                if data.len() < 3 {
                    return Err(Error::InvalidLength);
                }
                let message_id = Self::try_from(&data[..3])?;
                Ok((message_id, 3))
            }
        }
    }
}

manufacturer_id_enum!(
    /// NMEA manufacturer identifier
    enum ManufacturerId {
        Aar = "ASIAN AMERICAN RESOURCES",
        Ace = "AUTO-COMM ENGINEERING CORP.",
        Acr = "ACR ELECTRONICS, INC.",
        Acs = "ARCO SOLAR, INC.",
        Act = "ADVANCED CONTROL TECHNOLOGY",
        Adi = "ADITEL",
        Adn = "AD NAVIGATION",
        Agi = "AIRGUIDE INSTRUMENT CO.",
        Aha = "AUTOHELM OF AMERICA",
        Aap = "AIPHONE CORP.",
        Ald = "ALDEN ELECTRONICS, INC.",
        Amb = "AMBARELLA INC",
        Amc = "ALLTEK MARINE ELECTRONICS CORP.",
        Ami = "ADVANCED MARINE INSTRUMENTATION, LTD.",
        Amm = "AQUAMETRO MARINE GmbH",
        Amr = "AMR SYSTEMS",
        Amt = "AIRMAR TECHNOLOGY",
        And = "ANDREW CORPORATION",
        Ani = "AUTONAUTICAL INSTRUMENTAL S.L.",
        Ssn = "SEPTENTRIO",
        Tnl = "TRIMBLE NAVIGATION",
    }
);

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn from_bytes() {
        assert_eq!(
            ManufacturerId::Tnl,
            ManufacturerId::try_from("TNL".as_bytes()).unwrap()
        );
        assert_eq!(
            Err(Error::InvalidValue),
            ManufacturerId::try_from("XXX".as_bytes())
        );
    }

    #[test]
    fn name() {
        assert_eq!(ManufacturerId::Adi.name(), "ADITEL");
    }
}
