use crate::{hex_byte, Error};

mod manufacturer_id;
mod message_id;
mod talker_id;

pub use manufacturer_id::ManufacturerId;
pub use message_id::MessageId;
pub use talker_id::TalkerId;

fn calculate_checksum(bytes: &[u8]) -> u8 {
    bytes.iter().fold(0, |c, x| c ^ *x)
}

/// # NMEA sentence
///
///
#[derive(Debug)]
pub struct Sentence<'a> {
    /// Talker identifier
    pub talker_id: TalkerId,
    /// Message identifier
    pub message_id: Option<MessageId>,
    /// Sentence bytes
    pub data: &'a [u8],
}

impl<'a> Sentence<'a> {
    /// Parse bytes into a `Sentence`.
    /// Also returns number of used bytes.
    pub fn parse(sentence: &[u8]) -> Result<(Sentence, usize), Error> {
        if sentence.len() < 5 {
            return Err(Error::NotEnoughData);
        }
        if sentence[0] != b'$' {
            return Err(Error::InvalidStartToken);
        }

        let mut data_length = None;
        for (pos, byte) in sentence.iter().enumerate() {
            if *byte == b'*' {
                data_length = Some(pos);
                break;
            }
        }
        let (checksum, used_bytes) = if let Some(data_length) = data_length {
            const CHECKSUM_LEN: usize = 2;
            let checksum_end = data_length + 1 + CHECKSUM_LEN;
            if sentence.len() >= checksum_end {
                let checksum_data = &sentence[data_length + 1..checksum_end];
                (hex_byte(checksum_data), checksum_end)
            } else {
                (None, 0)
            }
        } else {
            return Err(Error::NotEnoughData);
        };

        let buffer = match (checksum, data_length) {
            (Some(checksum), Some(data_length)) => {
                let calculated_checksum = calculate_checksum(&sentence[1..data_length]);
                if checksum != calculated_checksum {
                    return Err(Error::InvalidChecksum);
                }
                &sentence[1..data_length + 1]
            }
            _ => {
                return Err(Error::NotEnoughData);
            }
        };
        let (talker_id, used) = TalkerId::from_bytes(buffer)?;
        let buffer = &buffer[used..];

        let proprietary = matches!(talker_id, TalkerId::Proprietary(_));
        let (message_id, used) = match proprietary {
            true => (None, 0),
            false => {
                let (message_id, used) = MessageId::from_bytes(buffer)?;
                (Some(message_id), used)
            }
        };
        let buffer = &buffer[used..];
        let data = if !proprietary {
            if buffer[0] != b',' {
                return Err(Error::MalformedSentence);
            }
            &buffer[1..]
        } else {
            buffer
        };

        Ok((
            Sentence {
                talker_id,
                message_id,
                data,
            },
            used_bytes,
        ))
    }
}

impl<'a> core::fmt::Display for Sentence<'a> {
    fn fmt(&self, f: &mut core::fmt::Formatter) -> core::fmt::Result {
        match (self.message_id, core::str::from_utf8(self.data)) {
            (Some(message_id), Ok(text)) => write!(f, "{} {} {}", self.talker_id, message_id, text),
            (None, Ok(text)) => write!(f, "{} {}", self.talker_id, text),
            (Some(message_id), Err(_)) => write!(f, "{} {}", self.talker_id, message_id),
            (None, Err(_)) => write!(f, "{}", self.talker_id),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::{manufacturer_id::ManufacturerId, MessageId, Sentence, TalkerId};

    #[test]
    fn parse_gp_gga() {
        let (sentence, _used) = Sentence::parse(
            "$GPGGA,132618.90,5823.5942478,N,01534.4679346,E,1,25,0.6,93.4541,M,30.3354,M,,*54"
                .as_bytes(),
        )
        .unwrap();
        assert_eq!(sentence.talker_id, TalkerId::Gps);
        assert_eq!(sentence.message_id, Some(MessageId::Gga));
    }

    #[test]
    fn parse_ptnl_avr() {
        let (sentence, _used) = Sentence::parse(
            "$PTNL,AVR,181059.6,+149.4688,Yaw,+0.0134,Tilt,,,60.191,3,2.5,6*00".as_bytes(),
        )
        .unwrap();
        assert_eq!(
            sentence.talker_id,
            TalkerId::Proprietary(ManufacturerId::Tnl)
        );
        assert_eq!(sentence.message_id, None);
    }

    #[test]
    fn parse_err_length() {
        if let Err(error) = Sentence::parse(
            "$GPGGA,132618.90,5823.5942478,N,01534.4679346,E,1,25,0.6,93.4541,M,30.3354,M,,*5"
                .as_bytes(),
        ) {
            assert_eq!(error, crate::Error::NotEnoughData);
        } else {
            assert!(false);
        }
    }
}
