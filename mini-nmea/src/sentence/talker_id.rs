use super::manufacturer_id::ManufacturerId;
use crate::Error;

/// # Talker identifier
///
/// Describes the source of the sentence
#[derive(Clone, Debug, PartialEq)]
pub enum TalkerId {
    /// BeiDou, China
    Beidou,
    /// Galileo, Europe
    Galileo,
    /// Global Positioning System (GPS), United States
    Glonass,
    /// NavIC or IRNSS (Indian Regional Navigation Satellite System), India
    Gnss,
    /// Global Navigation Satellite System (GNSS), generic term
    Gps,
    /// GLObal NAvigation Sattellite System (GLONASS), Russia
    NavIc,
    /// Quasi-Zenith Satellite System (QZSS), Japan
    Qzss,
    /// Unknown
    Unknown([u8; 2]),
    /// Proprietary
    Proprietary(ManufacturerId),
}

impl TalkerId {
    /// Parse bytes into `TalkerId`
    pub fn from_bytes(data: &[u8]) -> Result<(TalkerId, usize), Error> {
        if data.len() < 2 {
            return Err(Error::InvalidLength);
        }
        let proprietary = data[0] == b'P' || data[0] == b'p';
        let (talker_id, used) = if proprietary {
            if data.len() < 4 {
                return Err(Error::InvalidLength);
            }
            let (manufacturer_id, _) = ManufacturerId::from_bytes(&data[1..=3])?;
            (TalkerId::Proprietary(manufacturer_id), 4)
        } else {
            let talker_id = match &data[0..=1] {
                b"BD" => TalkerId::Beidou,
                b"GA" => TalkerId::Galileo,
                b"GB" => TalkerId::Beidou,
                b"GI" => TalkerId::NavIc,
                b"GL" => TalkerId::Glonass,
                b"GN" => TalkerId::Gnss,
                b"GP" => TalkerId::Gps,
                b"GQ" => TalkerId::Qzss,
                value => {
                    let mut unknown_talker_id = [0; 2];
                    unknown_talker_id.clone_from_slice(value);
                    TalkerId::Unknown(unknown_talker_id)
                }
            };
            (talker_id, 2)
        };

        Ok((talker_id, used))
    }
}

impl core::fmt::Display for TalkerId {
    fn fmt(&self, f: &mut core::fmt::Formatter) -> core::fmt::Result {
        match self {
            Self::Beidou => write!(f, "Beidou"),
            Self::Galileo => write!(f, "Galileo"),
            Self::Glonass => write!(f, "GLONASS"),
            Self::Gnss => write!(f, "GNSS"),
            Self::Gps => write!(f, "GPS"),
            Self::NavIc => write!(f, "NavIc"),
            Self::Qzss => write!(f, "QZSS"),
            Self::Unknown(talker_id) => {
                if let Ok(text) = core::str::from_utf8(talker_id) {
                    write!(f, "Unknown {}", text)
                } else {
                    write!(f, "Unknown")
                }
            }
            Self::Proprietary(manufacturer_id) => {
                write!(f, "{}", *manufacturer_id)
            }
        }
    }
}
