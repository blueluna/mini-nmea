//! # Mini-NMEA
//!
//! A `no_std` focused NMEA parser

#![no_std]
#![deny(missing_docs)]

mod error;
pub mod messages;
mod parse;
mod sentence;

pub use error::Error;
pub use messages::{
    FromSentence, GnssQualityIndicator, MagneticTrack, MessageContent, ModeIndicator,
    NauticalSpeed, Speed, Status, TrueTrack,
};
pub use parse::hex_byte;
pub use sentence::{MessageId, Sentence, TalkerId};
