/// Crate error type
#[derive(Debug, PartialEq)]
pub enum Error {
    /// Not enough data needed for the operation
    NotEnoughData,
    /// The thing was not found
    NotFound,
    /// The data or value is of invalid length
    InvalidLength,
    /// The value is invalid
    InvalidValue,
    /// The duration is invalid
    InvalidDuration,
    /// The sentence start token '$' is invalid
    InvalidStartToken,
    /// The sentence checksum token '*' is invalid
    InvalidChecksumToken,
    /// The sentence checksum is invalid
    InvalidChecksum,
    /// Could not parse sentence
    MalformedSentence,
    /// Failed to convert bytes to string
    InvalidString,
    /// Failed to parse value from string
    ParseFailed,
    /// Invalid message identifier
    InvalidMessageIdentifier,
    /// Invalid talker identifier
    InvalidTalkerIdentifier,
    /// The field is empty
    EmptyField,
}

impl core::fmt::Display for Error {
    /// Format error
    fn fmt(&self, f: &mut core::fmt::Formatter) -> core::fmt::Result {
        write!(f, "Error {:?}", self)
    }
}
