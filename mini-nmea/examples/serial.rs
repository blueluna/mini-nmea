use bytes::BytesMut;
use clap::{value_t, App, Arg};
use mini_nmea::{FromSentence, MessageContent, Sentence};
use tokio::io::{AsyncReadExt, BufReader};
use tokio_serial::SerialPortBuilderExt;

#[tokio::main]
async fn main() -> tokio_serial::Result<()> {
    let matches = App::new("nmea-serial")
        .version("0.1")
        .author("Erik Svensson <erik.public@gmail.com>")
        .about("Read NMEA-0183 on serial port")
        .arg(
            Arg::with_name("device")
                .value_name("DEV")
                .help("The serial device to use")
                .index(1)
                .required(true),
        )
        .arg(
            Arg::with_name("baudrate")
                .short("b")
                .long("baud")
                .help("The baud rate to use")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("v")
                .short("v")
                .multiple(true)
                .help("Sets the level of verbosity"),
        )
        .get_matches();

    let baud_rate = value_t!(matches, "baudrate", u32).unwrap_or(4800);

    let device = if let Some(device) = matches.value_of("device") {
        device
    } else {
        return Ok(());
    };

    let mut port = tokio_serial::new(device, baud_rate).open_native_async()?;

    #[cfg(unix)]
    port.set_exclusive(false)
        .expect("Unable to set serial port exclusive to false");

    let mut reader = BufReader::new(port);

    let mut buffer = BytesMut::with_capacity(1024);
    while let Ok(_) = reader.read_buf(&mut buffer).await {
        if let Some(offset) = buffer.iter().position(|b| *b == b'\n') {
            let line = buffer.split_to(offset + 1);
            match Sentence::parse(&line) {
                Ok((sentence, _)) => match MessageContent::from_sentence(&sentence) {
                    Ok(_msg) => {
                        println!("{:8} {}", sentence.talker_id.to_string(), _msg);
                    }
                    Err(_) => {
                        println!("{}", sentence)
                    }
                },
                Err(error) => {
                    eprintln!("Error {}", error);
                }
            }
        }
    }
    Ok(())
}
