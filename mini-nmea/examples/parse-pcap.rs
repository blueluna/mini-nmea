use bytes::BytesMut;
use chrono::{DateTime, TimeZone, Utc};
use clap::{App, Arg};
use mini_nmea::{FromSentence, MessageContent, Sentence};
use pcarp::{self, LinkType};
use pnet_packet::{
    ethernet::{EtherTypes, EthernetPacket},
    ip::IpNextHeaderProtocols,
    ipv4::Ipv4Packet,
    ipv6::Ipv6Packet,
    tcp::TcpPacket,
    udp::UdpPacket,
    PacketSize,
};
use std::{fs::File, time::SystemTime};

/// Crate error type
#[derive(Debug)]
pub enum Error {
    /// Data has invalid format
    InvalidFormat,
    /// Payload of invalid protocol
    InvalidProtocol,
    /// I/O error
    IoError(std::io::Error),
    /// Pcarp
    Pcarp(pcarp::Error),
}

impl std::fmt::Display for Error {
    /// Format error
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "Error {:?}", self)
    }
}

impl std::error::Error for Error {
    /// Get a descriptor for the error
    fn description(&self) -> &str {
        "Error"
    }
}

impl From<std::io::Error> for Error {
    /// Build error from I/O error
    fn from(error: std::io::Error) -> Self {
        Error::IoError(error)
    }
}

impl From<pcarp::Error> for Error {
    /// Build error from I/O error
    fn from(error: pcarp::Error) -> Self {
        Error::Pcarp(error)
    }
}

fn datetime_from_timestamp(timestamp: Option<SystemTime>) -> Option<DateTime<Utc>> {
    if let Some(timestamp) = timestamp {
        match timestamp.duration_since(std::time::SystemTime::UNIX_EPOCH) {
            Ok(duration) => {
                if let Ok(since) = chrono::Duration::from_std(duration) {
                    let dt = Utc.ymd(1970, 1, 1).and_hms(0, 0, 0) + since;
                    return Some(dt);
                }
            }
            Err(_) => (),
        }
    }
    None
}

fn handle_network_payload(_timestamp: &DateTime<Utc>, payload: &[u8]) -> Result<(), Error> {
    let mut buffer = BytesMut::from(payload);
    while let Some(offset) = buffer.iter().position(|b| *b == b'\n') {
        let line = buffer.split_to(offset + 1);
        match Sentence::parse(&line) {
            Ok((sentence, _)) => match MessageContent::from_sentence(&sentence) {
                Ok(_msg) => {
                    println!("{:8} {}", sentence.talker_id.to_string(), _msg);
                }
                Err(_) => {
                    println!("{}", sentence)
                }
            },
            Err(error) => {
                eprintln!("Error {}", error);
            }
        }
    }
    Ok(())
}

fn handle_ethernet_frame(timestamp: &DateTime<Utc>, payload: &[u8]) -> Result<(), Error> {
    let mut offset = 0;
    let ethernet = EthernetPacket::new(&payload[offset..]).ok_or(Error::InvalidFormat)?;
    offset += ethernet.packet_size();
    let (next, used) = match ethernet.get_ethertype() {
        EtherTypes::Ipv4 => {
            let ip = Ipv4Packet::new(&payload[offset..]).ok_or(Error::InvalidFormat)?;
            let length = (ip.get_header_length() * 4) as usize;
            (ip.get_next_level_protocol(), length)
        }
        EtherTypes::Ipv6 => {
            let ip = Ipv6Packet::new(&payload[offset..]).ok_or(Error::InvalidFormat)?;
            let length = ip.packet_size() - ip.get_payload_length() as usize;
            (ip.get_next_header(), length)
        }
        _ => {
            return Err(Error::InvalidProtocol);
        }
    };
    offset += used;
    let used = match next {
        IpNextHeaderProtocols::Udp => {
            let udp = UdpPacket::new(&payload[offset..]).ok_or(Error::InvalidFormat)?;
            udp.packet_size()
        }
        IpNextHeaderProtocols::Tcp => {
            let tcp = TcpPacket::new(&payload[offset..]).ok_or(Error::InvalidFormat)?;
            tcp.packet_size()
        }
        _ => {
            return Err(Error::InvalidProtocol);
        }
    };
    offset += used;
    handle_network_payload(timestamp, &payload[offset..])
}

fn handle_pcap_packet(packet: &pcarp::Packet) -> Result<(), Error> {
    if let (Some(interface), Some(timestamp), payload) = (
        packet.interface,
        datetime_from_timestamp(packet.timestamp),
        packet.data,
    ) {
        match interface.link_type {
            LinkType::ETHERNET => {
                handle_ethernet_frame(&timestamp, payload)?;
            }
            _ => (),
        }
    }
    Ok(())
}

fn main() -> Result<(), Error> {
    let matches = App::new("parse-pcap")
        .version("0.1")
        .author("Erik Svensson <erik.public@gmail.com>")
        .about("Parse NMEA-0183 from pcap file")
        .arg(
            Arg::with_name("file")
                .value_name("FILE")
                .help("File to parse")
                .index(1)
                .required(true),
        )
        .arg(
            Arg::with_name("v")
                .short("v")
                .multiple(true)
                .help("Sets the level of verbosity"),
        )
        .get_matches();

    let file_path = if let Some(file_path) = matches.value_of("file") {
        file_path
    } else {
        return Ok(());
    };
    let file = File::open(file_path)?;
    let mut capture = pcarp::Capture::new(file)?;

    loop {
        match capture.next() {
            Some(Ok(packet)) => handle_pcap_packet(&packet)?,
            Some(Err(error)) => {
                return Err(error.into());
            }
            None => {
                break;
            }
        };
    }
    Ok(())
}
