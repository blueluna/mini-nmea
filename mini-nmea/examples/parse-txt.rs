use bytes::BytesMut;
use clap::{App, Arg};
use mini_nmea::{FromSentence, MessageContent, Sentence};
use tokio::io::{AsyncReadExt, BufReader};

#[tokio::main]
async fn main() -> Result<(), tokio::io::Error> {
    let matches = App::new("parse-txt")
        .version("0.1")
        .author("Erik Svensson <erik.public@gmail.com>")
        .about("Parse NMEA-0183 from text file")
        .arg(
            Arg::with_name("file")
                .value_name("FILE")
                .help("File to parse")
                .index(1)
                .required(true),
        )
        .arg(
            Arg::with_name("v")
                .short("v")
                .multiple(true)
                .help("Sets the level of verbosity"),
        )
        .get_matches();

    let file_path = if let Some(file_path) = matches.value_of("file") {
        file_path
    } else {
        return Ok(());
    };
    let file = tokio::fs::File::open(file_path).await?;

    let mut reader = BufReader::new(file);

    let mut buffer = BytesMut::with_capacity(1024);
    while let Ok(size) = reader.read_buf(&mut buffer).await {
        if size == 0 {
            break;
        }
        while let Some(offset) = buffer.iter().position(|b| *b == b'\n') {
            let line = buffer.split_to(offset + 1);
            match Sentence::parse(&line) {
                Ok((sentence, _)) => match MessageContent::from_sentence(&sentence) {
                    Ok(_msg) => {
                        println!("{:8} {}", sentence.talker_id.to_string(), _msg);
                    }
                    Err(_) => {
                        println!("{}", sentence)
                    }
                },
                Err(error) => {
                    eprintln!("Error {}", error);
                }
            }
        }
    }
    Ok(())
}
